FROM node:13.12.0-alpine AS builder

# set working directory
WORKDIR /app
 
COPY package.json .
COPY package-lock.json .
COPY . .
RUN npm install && npm run-script build

FROM nginx:1.17.10-alpine
RUN apk add nano && apk add curl
#WORKDIR /usr/share/nginx/html
#RUN rm -rf ./*
COPY --from=builder /app/build /usr/share/nginx/html/
RUN rm -rf /etc/nginx/conf.d/default.conf
COPY ./nginx.conf /etc/nginx/conf.d

ENTRYPOINT ["nginx", "-g", "daemon off;"]