import './App.css';
import {AddTodo} from './todo/AddTodo'
function App() {
  return (
    <div className="App">
      <AddTodo/>
    </div>
  );
}

export default App;
