import { render, screen } from '@testing-library/react';
import {AddTodo} from './AddTodo';
 
test('renders learn placeholder', () => {
  render(<AddTodo />);
  const placeholder = screen.queryByPlaceholderText(/What needs to be done?/i);
  expect(placeholder).toBeInTheDocument() 
});  
test('renders learn placeholder is null', () => {
  render(<AddTodo />);
  const placeholder = screen.queryByPlaceholderText(/What needs to be done?/i);
  expect(placeholder).not.toBeNull();
});  