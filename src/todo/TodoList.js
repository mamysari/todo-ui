import React from 'react';
import {ListItem} from './ListItem'
export class TodoList extends React.Component {
    constructor(props){
        super(props);
     
        console.log("tODOlİST  cONS");
    }
    
    render(){
        return (
            <div>
                <ul className="todo-list">
                   {this.props.List.map(item => 
                   <ListItem key={item.id} item={item}/>)}
               </ul>
       
            </div> 
           );
    }
} 