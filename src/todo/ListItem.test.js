import { render, screen } from '@testing-library/react';
import {ListItem} from './ListItem';
 
test('renders ListItem', () => {
 
  const rendered = render(<ListItem item={{value:"Merhaba"}} />);
  const div =rendered.container.querySelector("div");
  expect(div.className).toBe("view")
});
