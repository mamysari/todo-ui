import axios from 'axios';
import React from 'react';
import { TodoList } from './TodoList';
export class AddTodo extends React.Component {
    constructor(props){
        super(props)
        this.handleOnKeyDown = this.handleOnKeyDown.bind(this);
        this.save = this.save.bind(this);
        this.getList = this.getList.bind(this);
        this.state ={
            List:[],
            value:''
        }
        this.getList()
    }
    handleOnKeyDown(e) {
      if (e.keyCode !== 13) {
          return;
      }
      e.preventDefault(); 
      this.save(); 
    }
    save (){
      axios({
        method:'post',
        url:'/api/todos',
        data : {
          value:this.state.value
        }
      }).then( ({data}) =>{
          if(data.status === 200)
          {
            this.getList();
          }
      }).catch(res=>{
          this.getList();
      })
    }
    getList (){
        axios({
            method:'get',
            url:'/api/todos',
         
          }).then( ({data}) =>{
              console.log(data);
              this.setState({
                  List:data.data,
                  value:''
              })
          }).catch(res=>{
         
          })
    }
    render(){
    return (
      <div> 
          <input    
          className="new-todo" placeholder="What needs to be done?" 
          autoFocus={true}
          value = {this.state.value}
          onChange = {e=> this.setState({value:e.target.value})}
          onKeyDown={ e => this.handleOnKeyDown(e) } />
          <TodoList List ={this.state.List}/>
      </div>
    )}
  }