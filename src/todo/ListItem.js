import React from 'react'; 
  
export function ListItem(props) {  
    return (
        <li>
            <div className="view"> 
            <label>
                { props.item.value}
             </label> 
            </div>
          </li>
    );
}   