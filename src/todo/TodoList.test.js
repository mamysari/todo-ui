import { render, screen } from '@testing-library/react';
import {TodoList} from './TodoList';
 
test('renders TodoList', () => {
    var List = [{
        id:1, value:"Merhaba"
    }]
  const rendered = render(<TodoList List={List} />);
  const ul =rendered.container.querySelector("ul");
  expect(ul.className).toBe("todo-list")
});
